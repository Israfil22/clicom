import {Plugin} from '../index'
import boxen from 'boxen'
import {Boxes} from 'cli-boxes'

const regExp = /^box=\[.+]$/i

const plugin: Plugin = {
    isMatch: (str) => regExp.test(str),
    executor: async (str) => {
        const command = /\[(.+)]/g.exec(str)

        if (!(command) || command[1] === undefined)
            throw Error('Invalid pattern')

        const params = command[1].split(',')
        if (params.length < 4)
            throw new Error('4 params required')

        const color = params[0] ? params[0] : 'blue'
        const style = params[1] ? params[1] : 'round'
        const padding = params[2] ? parseInt(params[2]) : 0
        const text = params.slice(3).join(',')

        console.log(
            boxen(text, {
                borderColor: color,
                borderStyle: style as keyof Boxes, // Boxen creator - faggot. He must use enums instead this shit.
                padding: padding,
                float: 'center',
                align: 'center'
            })
        )
    }
}

export default plugin

