/*
* W - WAIT
* DELAY - MS
* */

import {Plugin} from '../index'

const regExp = /^(w|wait)=[^\s]+$/i
const delay = (d: number) => new Promise(resolve => setTimeout(resolve, d))

const plugin: Plugin = {
    isMatch: (str) => regExp.test(str),
    executor: async (str) => {
        const del = str.replace(/\^/g, ' ').replace(/^.+=/g, '').trim()

        await delay(parseInt(del))
    }
}

export default plugin
