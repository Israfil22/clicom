import flushConsolePlugin from './flush-console'
import textLogoPlugin from './text-logo'
import waitPlugin from './wait'
import boxPlugin from './box'

export type Plugin = {
    isMatch: (flag: string) => boolean,
    executor: (flag: string) => Promise<void>
}

const flags = process.argv

const run = async (plugins: Array<Plugin>): Promise<void> => {
    for (const flag of flags) {
        for (const plugin of plugins) {
            if (plugin.isMatch(flag))
                await plugin.executor(flag)
        }
    }
}

export const DefaultPlugins = [
    flushConsolePlugin,
    textLogoPlugin,
    waitPlugin,
    boxPlugin
]

if (require.main?.id === module.id)
    run(DefaultPlugins)

export default run
