import {Plugin} from '../index'

const regExp = /^text-logo=.+$/i

const plugin: Plugin = {
    isMatch: (str) => regExp.test(str),
    executor: async (str) => {
        const phrase = str.replace(/^.+=/g, '').trim()
        const result =
            '='.repeat(phrase.length + 8 + 8 + 2 + 2) + '\n' +
            '||' + ' '.repeat(8) + phrase + ' '.repeat(8) + '||' + '\n' +
            '='.repeat(phrase.length + 8 + 8 + 2 + 2)

        console.log(result)
    }
}

export default plugin

