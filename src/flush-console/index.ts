import {Plugin} from '../index'


const plugin: Plugin = {
    isMatch: (str) => str === 'flush',
    executor: async () => {
        console.clear()
    }
}

export default plugin
