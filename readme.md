# Clicom

### When i can use it?
If you need a simple cli application, that execute your small js files as **one** long **command** with params. 
  
  
#### Example
If before building application you want:
1) Fetch some data from Internet and write part of it into configuration 
2) Flush your console
  
Example project structure:
```
-appRootPath
    - YOUR PROJECT
    - package.json
    - __clicom__ // Scripts folder
        - fetchdata.js // Your example script
    - cc.js // Entrypoint
```
  
  
###### **fetchdata.js**
```js
// Import some dependencies
const {join} = require('path')
const {writeFile} = require('fs/promises')
const {appRootPath} = require('./cfg')

// Main code
const mainThread = async (flag) => {
    // Parse argument from flag
    const url = flag.replace(/^prefetch=/g, '')

    // Get data from web
    const data = await fetch(url)
        .then(res => {
            return res.toJson()
        })
    
    // And write last information in file
    await writeFile(
        join(appRootPath, '/', 'lastdata'),
        data
    )
}

// Export this plugin 
module.exports = {
    // Function, that return boolean value (need execute 'executor' or not for flag in parametr)
    isMatch: (flag) => /^prefetch=/g.test(flag),
    executor: mainThread
}
```
  
  
###### **cc.js**
```js
const cc = require('clicom')

const myPlugins = [
    // Plugin from example above
    require('./__clicom__/fetchdata.js')
]

// Execute plugins with default plugings
cc.default(
    Object.assign( cc.DefaultPlugins, myPlugins)
)
```
  
  
###### **package.json**
```
"scripts": {
    //       (or cc.js)          (built-in plugin) (start project)          
    "run": "node cc prefetch=google.com flush && node ."
}
```


## Built-in plugins  
  
### Flush-console  
**Flag**: 'flush'  
**Args:** no  
Description:  
Just flush console  
  
### Wait  
**Flag**: 'wait' or 'w'   
**Args:** [1 - delay in ms]  
Description:  
Some pause with passed delay  
  
### Logo  
**Flag**: 'text-logo'  
**Args:** [1 - Words with not-space delimiter (^ char)]  
**Example**: text-logo=Hello^world  
Description:  
Output some data in console with pretty char border  
